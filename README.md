This module just adds the service description to the service list page (/contrat/services_list.php)

It was made in a quick and dirty way : don't take it as an example.
Our use case was that we have technical data in the description, and having it in this
listing speed up the check.
